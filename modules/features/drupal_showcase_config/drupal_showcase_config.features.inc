<?php
/**
 * @file
 * drupal_showcase_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupal_showcase_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
