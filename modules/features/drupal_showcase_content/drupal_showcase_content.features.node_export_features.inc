<?php
/**
 * @file
 * drupal_showcase_content.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function drupal_showcase_content_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'vid\' => \'1\',
      \'uid\' => \'1\',
      \'title\' => \'About\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'0\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'1253f885-d8a4-48b0-a3b1-f2b0e37e4634\',
      \'nid\' => \'1\',
      \'type\' => \'page\',
      \'language\' => \'und\',
      \'created\' => \'1407450169\',
      \'changed\' => \'1407450230\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'0bf93042-8f34-4f5c-a17d-4cc361ca21bf\',
      \'revision_timestamp\' => \'1407450230\',
      \'revision_uid\' => \'1\',
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => \'<p>This site is a demo site to showcase how to use several of the commonly used, and some not-so-commonly used, Drupal components. It also demonstrates many techniques and best-practices in building your Drupal sites.</p>\',
            \'summary\' => \'\',
            \'format\' => \'full_html\',
            \'safe_value\' => "<p>This site is a demo site to showcase how to use several of the commonly used, and some not-so-commonly used, Drupal components. It also demonstrates many techniques and best-practices in building your Drupal sites.</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'name\' => \'admin\',
      \'picture\' => \'0\',
      \'data\' => \'b:0;\',
      \'path\' => array(
        \'pid\' => \'1\',
        \'source\' => \'node/1\',
        \'alias\' => \'page/about\',
        \'language\' => \'und\',
      ),
      \'menu\' => array(
        \'link_title\' => \'About\',
        \'mlid\' => 0,
        \'plid\' => \'0\',
        \'menu_name\' => \'main-menu\',
        \'weight\' => \'0\',
        \'options\' => array(),
        \'module\' => \'menu\',
        \'expanded\' => \'0\',
        \'hidden\' => \'0\',
        \'has_children\' => \'0\',
        \'customized\' => 0,
        \'parent_depth_limit\' => 8,
        \'description\' => \'about this site\',
        \'enabled\' => 1,
      ),
      \'node_export_drupal_version\' => \'7\',
    ),
)',
);
  return $node_export;
}
