#!/bin/bash

if [ ! -f "settings.php" ]; then
  echo "You must run this from a Drupal site directory containing settings.php"
  exit 1
fi

if [ ! -d "modules/contrib" ]; then
  chmod +w .
  drush make --no-core --contrib-destination=. drush.make .
  chmod -w .
fi

drush site-install --site-name='SpinSpire Drupal Showcase'

#disable modules we don't like, and enable the ones we do like
drush pm-disable -y update,overlay,dashboard,comment,rdf,toolbar,shortcut,search
drush pm-enable  -y admin_menu,devel,module_filter,devel_generate,admin_menu_toolbar

#enable the showcase_bootstrap theme
drush -y en showcase_bootstrap

#enable the config feature
drush -y en drupal_showcase_config

#the feature needs reverting
#because certain variables go back to their default values
drush -y features-revert drupal_showcase_config

#add packaged content (node_export_features must be enabled BEFORE the content feature)
drush -y en node_export_features
drush -y en drupal_showcase_content

#generate articles
#drush generate-content --types=article 20 0

#enable the event feature
drush -y en drupal_showcase_event
#generate events
drush generate-content --types=event 20 0
